#!/bin/sh

ARENA_CLASS=ntu.csie.oop13spring.Volcano
PET1_CLASS=ntu.csie.oop13spring.FireDragon
PET2_CLASS=ntu.csie.oop13spring.IceFairy
PET3_CLASS=ntu.csie.oop13spring.FireDragon
PET4_CLASS=ntu.csie.oop13spring.IceFairy
PET5_CLASS=ntu.csie.oop13spring.FireDragon

java -jar hw4.jar $ARENA_CLASS $PET1_CLASS $PET2_CLASS $PET3_CLASS $PET4_CLASS $PET5_CLASS
