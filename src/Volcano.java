package ntu.csie.oop13spring;

import java.util.Random;

/**
 * Volcano is the arena where pets fight
 * It is a ring shape arena
 */
public class Volcano extends POOArena{
    final static int SIZE = 50;
    private char [][] map = new char[SIZE][SIZE];

    public Volcano(){ // Initialize the map that you can't stand in the center area
        for(int i = 0;i < SIZE;i++)
            for(int j = 0;j < SIZE;j++)
                if(i >= 17 && i <= 32 && j >= 17 && j <= 32)
                    map[i][j] = 'X';
                else
                    map[i][j] = '-';
    }

    public void setMap(int x, int y, char toSet){
        map[x][y] = toSet;
    }

    public char getMap(int x, int y){
        return map[x][y];
    }

    // The new version of addPet to randomly give the pets position and update the map
    // It will call super.addPet at the last
    public void addPet(POOPet p){
        char type;
        FireDragon f = null;
        IceFairy ice = null;
        if(p instanceof FireDragon){
            f = (FireDragon)p;
            type = 'F';
        }
        else{
            ice = (IceFairy)p;
            type = 'I';
        }

        Random random = new Random();
        int randomX, randomY;

        while(true){
            randomX = random.nextInt(50);
            randomY = random.nextInt(50);
            if(map[randomX][randomY] == '-'){
                map[randomX][randomY] = type;
                if(type == 'F')
                    f.setCoordinate(randomX, randomY);
                else
                    ice.setCoordinate(randomX, randomY);
                break;
            }
        }
        if(type == 'F')
            super.addPet(f);
        else
            super.addPet(ice);
    }

    // One round that pets fight to each other
    // It check is there only one pet alive first
    // Then, every pets can move and take action one time
    public boolean fight(){
        POOPet [] ary = getAllPets();
        FireDragon tmpF = null;
        IceFairy tmpI = null;
        int aliveCount = 0, aliveNum = 0;

        for(int i = 0;i < ary.length;i++){ // Check is there only one pet alive
            if(ary[i].getHP() != 0){
                aliveCount++;
                aliveNum = i;
            }
        }
        if(aliveCount == 1){
            System.out.println("");
            System.out.println("Winner is "+ary[aliveNum]);
            return false;
        }

        for(int i = 0;i < ary.length;i++){ // one move and one action for every pet
            if(ary[i].getHP() != 0){ // Check is the pet alive
                POOAction action = null;
                if(ary[i] instanceof FireDragon){
                    tmpF = (FireDragon)ary[i];
                    tmpF.move(this);
                    action = tmpF.act(this);
                }
                else{
                    tmpI = (IceFairy)ary[i];
                    tmpI.move(this);
                    action = tmpI.act(this);
                }

                FireDragon toAttackF = null; // Invoke the action returned
                IceFairy toAttackI = null;
                if(action.dest instanceof FireDragon){
                    toAttackF = (FireDragon)action.dest;
                    if(toAttackF != null){
                        action.skill.act(toAttackF);
                        if(toAttackF.getHP() == 0){
                            System.out.println(toAttackF.getName() + " is died");
                            map[toAttackF.getCoordinate().getX()][toAttackF.getCoordinate().getY()] = 'D';
                        }
                    }
                }
                else{
                    toAttackI = (IceFairy)action.dest;
                    if(toAttackI != null){
                        action.skill.act(toAttackI);
                        if(toAttackI.getHP() == 0){
                            System.out.println(toAttackI.getName() + " is died");
                            map[toAttackI.getCoordinate().getX()][toAttackI.getCoordinate().getY()] = 'D';
                        }
                    }
                }

            }
        }

        return true;
    }

    // Print the map
    public void print(){
        for(int i = 0;i < SIZE;i++){
            for(int j = 0;j < SIZE;j++)
                System.out.print(map[i][j]+" ");
            System.out.println();
        }
    }

    // Show the current status include name, HP, MP, AGI for every pet
    public void show(){
        POOPet [] ary = getAllPets();
        System.out.println("");
        System.out.println("Current status:");
        for(int i = 0;i < ary.length;i++){
            System.out.println(i + ".");
            System.out.println("Name: " + ary[i].getName());
            System.out.println("HP: " + ary[i].getHP());
            System.out.println("MP: " + ary[i].getMP());
            System.out.println("AGI: " + ary[i].getAGI());
        }
    }

    // Return the coordinate of the pet
    public POOCoordinate getPosition(POOPet p){
        if(p instanceof FireDragon){
            FireDragon f = (FireDragon)p;
            return new Coordinate(f.getCoordinate());
        }
        else{
            IceFairy ice = (IceFairy)p;
            return new Coordinate(ice.getCoordinate());
        }
    }

    // For testing
    public static void main(String [] argv){
        Volcano v = new Volcano();
        v.addPet(new FireDragon("F1"));
        v.addPet(new FireDragon("F2"));
        v.addPet(new FireDragon("F3"));
        v.addPet(new IceFairy("I1"));
        v.addPet(new IceFairy("I2"));
        while(v.fight()){
            v.show();
        }
    }
}
