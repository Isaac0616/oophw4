package ntu.csie.oop13spring;

import java.util.*;

/**
 * IceFairy has large attack and moving range but the damage is lower.
 */
public class IceFairy extends POOPet{
    private Coordinate coordinate = new Coordinate();
    private IceFairyIceBall iceball = new IceFairyIceBall();
    private IceFairyNormalAttack normalAttack = new IceFairyNormalAttack();

    // Constructor. It will ask you to enter the name of the class and the ability.
    // You can enter 0 to have the random ability points.
    IceFairy(){
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int tmp;

        System.out.println("Please enter a name for the IceFairy.");
        System.out.print("Name:");
        setName(scanner.next());

        System.out.println("Please enter the ability for the IceFairy (Range: 1 ~ 1023).");
        System.out.println("You can enter 0 for a random number form 350 ~ 700");
        System.out.print("HP:");
        tmp = scanner.nextInt();
        if(tmp == 0)
            setHP(random.nextInt(351) + 350);
        else
            setHP(tmp);
        System.out.print("MP:");
        tmp = scanner.nextInt();
        if(tmp == 0)
            setMP(random.nextInt(351) + 350);
        else
            setMP(tmp);
        System.out.print("AGI:");
        tmp = scanner.nextInt();
        if(tmp == 0)
            setAGI(random.nextInt(351) + 350);
        else
            setAGI(tmp);
    }

    // Constructor for testing. Creating every fields automatically
    IceFairy(String n){
        Random random = new Random();
        setName(n);
        setHP(random.nextInt(351) + 350);
        setMP(random.nextInt(351) + 350);
        setAGI(random.nextInt(351) + 350);
    }

    public Coordinate getCoordinate(){
        return new Coordinate(coordinate);
    }

    public void setCoordinate(int x, int y){
        coordinate.setX(x);
        coordinate.setY(y);
    }

    // In this method, it will ask you to choose an action and the pet you want to apply the action on.
    protected POOAction act(POOArena arena){
        Scanner scanner = new Scanner(System.in);
        Volcano v = (Volcano)arena;
        POOPet [] ary = arena.getAllPets();
        ArrayList<POOPet> toAttack = new ArrayList<POOPet>(0);
        int actionNumber, distance, enemyNumber;
        FireDragon destF = null;
        IceFairy destI = null;
        POOSkill skill = null;
        char type = 0;

        while(true){ // It won't stop if you choose an action but nobody is in you range.
            System.out.println("Please choose an action.");
            System.out.println("1. iceball (range 20)");
            System.out.println("2. normal attack  (range 5)");
            System.out.println("3. nothing");
            System.out.print("Ation number:");
            actionNumber = scanner.nextInt(); // The choices.

            if(actionNumber == 1){ // action 1
                for(int i = 0;i < ary.length;i++){ // Find the alive pets in the range of actoin 1
                    Coordinate tmpC;               // and give them a sirial number. Also, add qualiied
                    FireDragon tmpF = null;        // pets into a list for recovering there notion on map.
                    IceFairy tmpI = null;
                    if(ary[i] instanceof FireDragon){
                        tmpF = (FireDragon)ary[i];
                        tmpC = tmpF.getCoordinate();
                        type = 'F';
                    }
                    else{
                        tmpI = (IceFairy)ary[i];
                        tmpC = tmpI.getCoordinate();
                        type = 'I';
                    }
                    distance = coordinate.distance(tmpC);

                    if(distance != 0 && distance <= 20 && v.getMap(tmpC.getX(), tmpC.getY()) != 'D'){
                        v.setMap(tmpC.getX(), tmpC.getY(), (char)(toAttack.size() + 48));
                        if(type == 'F')
                            toAttack.add(tmpF);
                        else
                            toAttack.add(tmpI);
                    }
                }
                if(toAttack.size() == 0){ // No one in the range.
                    System.out.println("There are no enemy in your attack range.");
                }
                else{ // It ask you to choose a target according to serial number.
                    v.print();
                    System.out.print("Please choose an enemy:");
                    enemyNumber = scanner.nextInt();
                    if(toAttack.get(enemyNumber) instanceof FireDragon){
                        destF = (FireDragon)toAttack.get(enemyNumber);
                        type = 'F';
                    }
                    else{
                        destI = (IceFairy)toAttack.get(enemyNumber);
                        type = 'I';
                    }
                    skill = new IceFairyIceBall();

                    for(int i = 0;i < toAttack.size();i++){ // Recover the notion of pets on map.
                        if(toAttack.get(i) instanceof FireDragon){
                            Coordinate tmpC = ((FireDragon)toAttack.get(i)).getCoordinate();
                            v.setMap(tmpC.getX(), tmpC.getY(), 'F');
                        }
                        else{
                            Coordinate tmpC = ((IceFairy)toAttack.get(i)).getCoordinate();
                            v.setMap(tmpC.getX(), tmpC.getY(), 'I');
                        }
                    }
                    break;
                }
            }
            else if(actionNumber == 2){ // action 2
                for(int i = 0;i < ary.length;i++){
                    Coordinate tmpC;
                    FireDragon tmpF = null;
                    IceFairy tmpI = null;
                    if(ary[i] instanceof FireDragon){
                        tmpF = (FireDragon)ary[i];
                        tmpC = tmpF.getCoordinate();
                        type = 'F';
                    }
                    else{
                        tmpI = (IceFairy)ary[i];
                        tmpC = tmpI.getCoordinate();
                        type = 'I';
                    }
                    distance = coordinate.distance(tmpC);

                    if(distance != 0 && distance <= 5 && v.getMap(tmpC.getX(), tmpC.getY()) != 'D'){
                        v.setMap(tmpC.getX(), tmpC.getY(), (char)(toAttack.size() + 48));
                        if(type == 'F')
                            toAttack.add(tmpF);
                        else
                            toAttack.add(tmpI);
                    }
                }
                if(toAttack.size() == 0){
                    System.out.println("There are no enemy in your attack range.");
                }
                else{
                    v.print();
                    System.out.print("Please choose an enemy:");
                    enemyNumber = scanner.nextInt();
                    if(toAttack.get(enemyNumber) instanceof FireDragon){
                        destF = (FireDragon)toAttack.get(enemyNumber);
                        type = 'F';
                    }
                    else{
                        destI = (IceFairy)toAttack.get(enemyNumber);
                        type = 'I';
                    }
                    skill = new IceFairyNormalAttack();

                    for(int i = 0;i < toAttack.size();i++){
                        if(toAttack.get(i) instanceof FireDragon){
                            Coordinate tmpC = ((FireDragon)toAttack.get(i)).getCoordinate();
                            v.setMap(tmpC.getX(), tmpC.getY(), 'F');
                        }
                        else{
                            Coordinate tmpC = ((IceFairy)toAttack.get(i)).getCoordinate();
                            v.setMap(tmpC.getX(), tmpC.getY(), 'I');
                        }
                    }
                    break;
                }
            }
            else if(actionNumber == 3){ // action 3: do nothing
                break;
            }
        }

        int x = coordinate.getX(); // Recover the notion of the map from @.
        int y = coordinate.getY();
        v.setMap(x, y, 'I');

        POOAction returnAction = new POOAction(); // Prepare the object to return.
        returnAction.skill = skill;
        if(type == 'F')
            returnAction.dest = destF;
        else
            returnAction.dest = destI;
        return returnAction;
    }

    // In this method, you can use the direction + steps to move arround the map.
    // You can stop by entering 0 as the direction.
    // The direction is N(North), E(East), S(South), W(West).
    // The current position is marked by @.
    public POOCoordinate move(POOArena arena){
        Scanner scanner = new Scanner(System.in);
        Volcano v = (Volcano)arena;
        int steps = getAGI()/100 + 7, toMove, x, y, newPosition; // The steps which you can move
        String direction;                                        // is 1% of your AGI + 7.

        x = coordinate.getX(); // Mark current position by @.
        y = coordinate.getY();
        v.setMap(x, y, '@');

        while(steps > 0){ // You can move until your left steps become 0.
            System.out.println("");
            v.print();
            System.out.println("You are at @ of current map.");
            System.out.println("You can move "+steps+" steps.");

            while(true){
                System.out.println("Please enter the direction(E, W, S, N) and distance you want to move or enter 0 to stop.");
                direction = scanner.next();
                if(direction.equals("0")){ // Stop
                    return coordinate;
                }
                toMove = scanner.nextInt();
                if(steps - toMove < 0){ // Your ramaining steps is not enough.
                    System.out.println("You can't move that far.");
                    continue;
                }

                if(direction.equals("S")){ // Move South
                    newPosition = x + toMove;
                    if(newPosition <= 49 && v.getMap(newPosition, y) == '-'){
                        v.setMap(x, y, '-');
                        v.setMap(newPosition, y, '@');
                        setCoordinate(newPosition, y);
                        x = newPosition;
                        steps -= toMove;
                        break;
                    }
                    else{
                        System.out.println("You can't move there.");
                    }
                }
                else if(direction.equals("N")){ // Move North
                    newPosition = x - toMove;
                    if(newPosition >= 0 && v.getMap(newPosition, y) == '-'){
                        v.setMap(x, y, '-');
                        v.setMap(newPosition, y, '@');
                        setCoordinate(newPosition, y);
                        x = newPosition;
                        steps -= toMove;
                        break;
                    }
                    else{
                        System.out.println("You can't move there.");
                    }
                }
                else if(direction.equals("E")){ // Move East
                    newPosition = y + toMove;
                    if(newPosition <= 49 && v.getMap(x, newPosition) == '-'){
                        v.setMap(x, y, '-');
                        v.setMap(x, newPosition, '@');
                        setCoordinate(x, newPosition);
                        y = newPosition;
                        steps -= toMove;
                        break;
                    }
                    else{
                        System.out.println("You can't move there.");
                    }
                }
                else if(direction.equals("W")){ // Move West
                    newPosition = y - toMove;
                    if(newPosition >= 0 && v.getMap(x, newPosition) == '-'){
                        v.setMap(x, y, '-');
                        v.setMap(x, newPosition, '@');
                        setCoordinate(x, newPosition);
                        y = newPosition;
                        steps -= toMove;
                        break;
                    }
                    else{
                        System.out.println("You can't move there.");
                    }
                }
            }
        }
        System.out.println("");
        v.print();

        return coordinate;
    }

    public String toString(){
        return getName();
    }

    class IceFairyIceBall extends POOSkill{ //IceFairy's skill IceBall. It make (100 + 30% of MP) points
        public void act(POOPet pet){        //damage, and the attack range is 20.
            int hp = pet.getHP();
            if (hp - 100 - (int)(getMP()*0.3) > 0)
                pet.setHP(hp - 100 - (int)(getMP()*0.3));
            else
                pet.setHP(0);

            System.out.println("You have used iceball to attack " + pet.getName());
            System.out.println(pet.getName() + "'s HP decrease from " + hp + " to " + pet.getHP());
        }
    }

    class IceFairyNormalAttack extends POOSkill{ //IceFairy's normal attack. Fix damage 100 points and the
        public void act(POOPet pet){             //range is 5.
            int hp = pet.getHP();
            if (hp - 100 > 0)
                pet.setHP(hp - 100);
            else
                pet.setHP(0);

            System.out.println("You have used normal attack to attack " + pet.getName());
            System.out.println(pet.getName() + "'s HP decrease from " + hp + " to " + pet.getHP());
        }
    }
}
