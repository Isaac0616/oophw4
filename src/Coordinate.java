package ntu.csie.oop13spring;

/**
 * The concrete class of POOCoordinate which implement equals and add some access method
 */
public class Coordinate extends POOCoordinate{
    public Coordinate(){}
    public Coordinate(Coordinate input){
        x = input.getX();
        y = input.getY();
    }

    public void setX(int input){
        x = input;
    }

    public void setY(int input){
        y = input;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    // Return the distance of two points which is defined by
    // the distance of x coordinate + the distance of y coordinate
    public int distance(Coordinate input){
        return (Math.abs(x - input.getX()) + Math.abs(y - input.getY()));
    }

    public boolean equals(POOCoordinate other){
        Coordinate otherCoordinate = (Coordinate)other;
        if(x == otherCoordinate .x && y == otherCoordinate.y)
            return true;
        else
            return false;
    }
}
